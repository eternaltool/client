﻿using System.Dynamic;
using System.Threading.Tasks;
using ProtoBuf;

namespace MunchausenServer
{
    [ProtoContract]
    public class IsValidAssemblyRequest
    {
        [ProtoMember(1)]
        public string assemblyHash { get; set; }

        [ProtoMember(2)]
        public string gameHash { get; set; }
    }

    [ProtoContract]
    public class IsValidAssemblyResponse
    {
        [ProtoMember(1)]
        public bool isValid { get; set; }

        [ProtoMember(2)]
        public bool hasAssembly { get; set; }

        [ProtoMember(3)]
        public byte[] validAssembly { get; set; }
    }


    [ProtoContract]
    public class GetAssemblyRequest
    {
        [ProtoMember(1)]
        public byte[] gameZip { get; set; }
    }

    [ProtoContract]
    public class GetAssemblyResponse
    {
        [ProtoMember(1)]
        public byte[] validAssembly { get; set; }
    }

    
}