﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Flurl.Http;
using MunchausenServer;
using Murmur;
using Newtonsoft.Json.Bson;
using ProtoBuf;

namespace ET.Client.munchausen
{
    public class RemoteMuchausen
    {
        ET.SharedData.config.Config conf = ET.SharedData.config.Config.Load();

        public async Task<IsValidAssemblyResponse> IsValidAssemly()
        {
            Console.WriteLine("making request");



            var gameHash = MurmurHash.Create128(seed: 97647574, managed: true, preference:AlgorithmPreference.X64);

            var coreBlock = File.ReadAllBytes(conf.hook.coreDll);
            var phienixBlock = File.ReadAllBytes(conf.hook.phoenixDll);
            var sausageBlock = File.ReadAllBytes(conf.hook.sausageDll);

            gameHash.TransformBlock(coreBlock, 0, coreBlock.Length, coreBlock, 0);
            gameHash.TransformBlock(phienixBlock, 0, phienixBlock.Length, phienixBlock, 0);
            gameHash.TransformFinalBlock(sausageBlock, 0, sausageBlock.Length);

            Console.WriteLine("GameHash: " + Convert.ToBase64String(gameHash.Hash));

            var stream = new MemoryStream();
            var request = new IsValidAssemblyRequest { gameHash = Convert.ToBase64String(gameHash.Hash)};

            Serializer.Serialize(stream, request);
            stream.Position = 0;
            var responseBytes = await $"{conf.server.apiServer}/IsValidAssembly"
                .PostAsync(new StreamContent(stream))
                .ReceiveStream();

            var resp = Serializer.Deserialize<IsValidAssemblyResponse>(responseBytes);

            Console.WriteLine($"hasAssmebly: {resp.hasAssembly}");
            Console.WriteLine($"isValid: {resp.isValid}");
            Console.WriteLine($"size : {resp.validAssembly?.Length}");

            return resp;
        }

        public async Task<GetAssemblyResponse> GetAssemly()
        {
            Console.WriteLine("making request");
            GetAssemblyResponse resp;
            using (var tempFiles = new TempFileCollection())
            {
                var zip = tempFiles.AddExtension("zip", false);
                ZipFile.CreateFromDirectory(conf.hook.dllPath, zip);

                 var stream = new MemoryStream();
                 var request = new GetAssemblyRequest { gameZip = File.ReadAllBytes(zip)};
                Serializer.Serialize(stream, request);
                stream.Position = 0;
                var responseBytes = await $"{conf.server.apiServer}/GetAssembly"
                    .PostAsync(new StreamContent(stream))
                    .ReceiveStream();

            resp = Serializer.Deserialize<GetAssemblyResponse>(responseBytes);

            Console.WriteLine($"size : {resp.validAssembly?.Length}");

                tempFiles.Delete();
            }

           

            return resp;
        }
    }
}
