﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using ET.Client.hook;

namespace client.hook
{
    public static class HookUtil
    {
        public static void Inject(HookConfig conf)
        {
            //Logger.LogDebug(state.GameType, $"Injecting injector-dll.dll, attempt #{state.InjectionAttempt}");
            var processes = Process.GetProcessesByName("Eternal");
            if (processes.Length == 1)
            {
                Console.WriteLine("Doing injection");
                foreach (ProcessModule module in processes[0].Modules) Console.WriteLine(module.FileName);
                var injectionLibrary = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "injector-dll.dll");
                Console.WriteLine(injectionLibrary);
                // processes[0].Kill();
                var result = DllInjector.InjectDll((uint) processes[0].Id, injectionLibrary, out _);
                Console.WriteLine(result);
                foreach (ProcessModule module in processes[0].Modules) Console.WriteLine(module.FileName);
            }
        }
    }
}